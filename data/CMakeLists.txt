list(APPEND DATA_FILES
    ${CMAKE_CURRENT_SOURCE_DIR}/poisson3d/trilinos.txt
    ${CMAKE_CURRENT_SOURCE_DIR}/poisson3d/amgcl-vexcl-cuda.txt
    ${CMAKE_CURRENT_SOURCE_DIR}/poisson3d/petsc.txt
    ${CMAKE_CURRENT_SOURCE_DIR}/poisson3d/amgcl.txt
    ${CMAKE_CURRENT_SOURCE_DIR}/poisson3d/amgcl-vexcl-opencl.txt
    ${CMAKE_CURRENT_SOURCE_DIR}/poisson3d/cusp.txt
    ${CMAKE_CURRENT_SOURCE_DIR}/poisson3d/amgcl-cuda.txt
    ${CMAKE_CURRENT_SOURCE_DIR}/block4/amgcl-schur-cuda.txt
    ${CMAKE_CURRENT_SOURCE_DIR}/block4/amgcl-schur-vexcl-opencl.txt
    ${CMAKE_CURRENT_SOURCE_DIR}/block4/trilinos.txt
    ${CMAKE_CURRENT_SOURCE_DIR}/block4/amgcl-vexcl-cuda.txt
    ${CMAKE_CURRENT_SOURCE_DIR}/block4/petsc.txt
    ${CMAKE_CURRENT_SOURCE_DIR}/block4/amgcl-schur-vexcl-cuda.txt
    ${CMAKE_CURRENT_SOURCE_DIR}/block4/amgcl.txt
    ${CMAKE_CURRENT_SOURCE_DIR}/block4/amgcl-schur.txt
    ${CMAKE_CURRENT_SOURCE_DIR}/block4/amgcl-vexcl-opencl.txt
    )

add_custom_command(OUTPUT ${CMAKE_BINARY_DIR}/perf_plot.pdf
    COMMAND ./plot.py -o ${CMAKE_BINARY_DIR}/perf_plot.pdf
    WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
    DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/plot.py ${DATA_FILES}
    )

add_custom_target(perf_plot DEPENDS ${CMAKE_BINARY_DIR}/perf_plot.pdf)

add_custom_command(OUTPUT ${CMAKE_BINARY_DIR}/perf_table.tex
    COMMAND ./table.py > ${CMAKE_BINARY_DIR}/perf_table.tex
    WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
    DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/table.py ${DATA_FILES}
    )

add_custom_target(perf_table DEPENDS ${CMAKE_BINARY_DIR}/perf_table.tex)

add_dependencies(amgcl_pdf perf_plot perf_table)
