#!/usr/bin/python

import sys, argparse

parser = argparse.ArgumentParser(sys.argv[0])
parser.add_argument('-o', dest='o', help='Output file name')
args = parser.parse_args(sys.argv[1:])

if args.o is not None:
    import matplotlib
    matplotlib.use('Agg')

from pylab import *

#rc('text',   usetex=True)
#rc('font',   family='serif')
#rc('font',   serif='cm')
#rc('font',   size=20)
#rc('legend', fontsize=18)

figure(figsize=(8,12))

label = dict(
        poisson3d={},
        block4={
            'amgcl'        : 'AMGCL (block)',
            'trilinos'     : 'Trilinos (block)',
            'petsc'        : 'PETSC (split)',
            'amgcl-schur'  : 'AMGCL (split)',
            'amgcl-scalar' : 'AMGCL (scalar)'
            }
        )

libraries = dict(
        block4=('amgcl', 'trilinos', 'petsc', 'amgcl-schur', 'amgcl-scalar'),
        poisson3d=('AMGCL', 'Trilinos', 'PETSC')
        )

problem_title = dict(
        poisson3d='Poisson in 3D cube',
        block4='Navier-Stokes in 3D cylinder')

for k,problem in enumerate(('poisson3d', 'block4')):
    for r,frac in enumerate(('total', 'setup', 'solve')):
        subplot2grid((3,2),(r,k))
        for lib in libraries[problem]:
            data = loadtxt('{}/{}.txt'.format(problem, lib.lower()), delimiter=' ', dtype=dict(
                names=('np', 'size', 'iters', 'setup', 'solve'),
                formats=('i4', 'i4', 'i4', 'f8', 'f8'))
            )

            np = data['np']
            cores = unique(sorted(data['np']))
            setup = empty(cores.shape)
            solve = empty(cores.shape)

            for i,n in enumerate(cores):
                setup[i] = median(data[np==n]['setup'])
                solve[i] = median(data[np==n]['solve'])

            total = setup + solve

            loglog(cores, eval(frac), marker='o', label=label[problem].get(lib, lib))

        xticks([1, 2, 4, 8, 16])
        gca().get_xaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
        legend()

        if k == 0: ylabel('{} time'.format(frac.capitalize()))
        if r == 0: title('{}'.format(problem_title[problem]))
        if r == 2: xlabel('Cores/MPI processes')

tight_layout()

if args.o is None:
    show()
else:
    savefig(args.o)

