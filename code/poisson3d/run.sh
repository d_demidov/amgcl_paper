#!/bin/bash

export OCL_MAX_DEVICES=1

rm *.txt


# --- GPU runs -----------------------------------------------------------
for i in `seq 5`; do
    echo -----------------------------------------------------------------
    echo --- i=$i
    echo -----------------------------------------------------------------
    ./poisson3d_cusp
    ./poisson3d_amgcl_cuda
    ./poisson3d_amgcl_vexcl_cuda
    ./poisson3d_amgcl_vexcl_opencl
done

# --- CPU runs -----------------------------------------------------------
for n in 1 2 4 8 16; do
    for i in `seq 5`; do
        echo -----------------------------------------------------------------
        echo --- CORES=$n, i=$i
        echo -----------------------------------------------------------------
        OMP_NUM_THREADS=$n OMP_PLACES=sockets numactl -l ./poisson3d_amgcl
        OMP_NUM_THREADS=1 mpirun -np $n ./poisson3d_petsc
        OMP_NUM_THREADS=1 mpirun -np $n ./poisson3d_trilinos
    done
done
