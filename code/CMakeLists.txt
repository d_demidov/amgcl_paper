include_directories(${CMAKE_CURRENT_SOURCE_DIR})

find_package(OpenMP)
find_package(MPI)

# --- Boost ----------------------------------------------------------------
find_package(Boost COMPONENTS program_options)

add_library(program_options INTERFACE)
target_include_directories(program_options INTERFACE ${Boost_INCLUDE_DIRS})
target_link_libraries(program_options INTERFACE ${Boost_PROGRAM_OPTIONS_LIBRARY})

# --- Trilinos -------------------------------------------------------------
if (DEFINED ENV{TRILINOS_DIR})
    add_library(trilinos_target INTERFACE)
    target_compile_options(trilinos_target INTERFACE ${OpenMP_CXX_FLAGS})
    target_include_directories(trilinos_target INTERFACE
        ${MPI_CXX_INCLUDE_PATH}
        $ENV{TRILINOS_DIR}/include
        )
    target_link_libraries(trilinos_target INTERFACE
        ${MPI_CXX_LIBRARIES}
        ${OpenMP_CXX_FLAGS})
    foreach(lib 
            aztecoo
            epetra
            epetraext
            ml
            teuchoscore
            teuchosparameterlist
            triutils
            )
        find_library(TRILINOS_${lib}_LIBRARY ${lib} PATHS ENV TRILINOS_DIR PATH_SUFFIXES lib)
        target_link_libraries(trilinos_target INTERFACE ${TRILINOS_${lib}_LIBRARY})
    endforeach()
endif()

# --- PETSC ----------------------------------------------------------------
if (DEFINED ENV{PETSC_DIR})
    add_library(petsc_target INTERFACE)
    target_compile_options(petsc_target INTERFACE ${OpenMP_CXX_FLAGS})
    target_include_directories(petsc_target INTERFACE
        ${MPI_CXX_INCLUDE_PATH}
        $ENV{PETSC_DIR}/include
        )
    find_library(PETSC_LIBRARY petsc PATHS ENV PETSC_DIR PATH_SUFFIXES lib)
    target_link_libraries(petsc_target INTERFACE
        ${MPI_CXX_LIBRARIES}
        ${OpenMP_CXX_FLAGS}
        ${PETSC_LIBRARY})
endif()

# --- AMGCL ----------------------------------------------------------------
find_package(amgcl)
get_property(AMGCL_INCLUDE_DIRS TARGET amgcl::amgcl PROPERTY INTERFACE_INCLUDE_DIRECTORIES)

# --- VexCL ----------------------------------------------------------------
find_package(VexCL)

# --- CUDA -----------------------------------------------------------------
find_package(CUDA)
if (CUDA_FOUND)
    cuda_select_nvcc_arch_flags(CUDA_ARCH_FLAGS Auto)

    list(APPEND CUDA_NVCC_FLAGS
        ${CUDA_ARCH_FLAGS} -Wno-deprecated-gpu-targets)

    if (CMAKE_CXX_COMPILER_ID MATCHES "GNU")
        list(APPEND CUDA_NVCC_FLAGS
            -Xcompiler -std=c++03
            -Xcompiler -Wno-vla
            -Xcompiler -fopenmp
            )
    endif()

    add_library(cusp_target INTERFACE)
    target_include_directories(cusp_target INTERFACE
        ${CMAKE_CURRENT_SOURCE_DIR}/cusplibrary)
endif()

# --------------------------------------------------------------------------
add_subdirectory(poisson3d)
add_subdirectory(block4)
