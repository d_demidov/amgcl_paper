\documentclass[final,a4paper]{siamart1116}

\usepackage{graphicx}
\usepackage{xspace}
\usepackage{inconsolata}
\usepackage{listings}
\lstset{
    language=C++,
    basicstyle=\footnotesize\ttfamily,
    commentstyle=\rmfamily,
    columns=flexible,
    showstringspaces=false,
    numbers=left,
    numberstyle=\tiny,
    frame=lines,
    xleftmargin=2em,framexleftmargin=2em,
    escapechar=$
    }
\usepackage{cleveref}
\crefname{lstlisting}{listing}{listings}
\Crefname{lstlisting}{Listing}{Listings}

\newcommand{\code}[1]{\lstinline|#1|}
\newcommand{\addpp}[1]{{#1\nolinebreak[4]\hspace{-.05em}\raisebox{.4ex}{\tiny\bf ++}}\xspace}
\newcommand{\CXX}{\addpp{C}}
\newcommand{\www}[1]{\href{#1}{#1}}

\author{Denis Demidov}
\title{AMGCL: an Efficient, Flexible, and Extensible Algebraic Multigrid Implementation}
\author{Denis Demidov \footnotemark[2]\ \footnotemark[3]}

\begin{document}

\maketitle

\renewcommand{\thefootnote}{\fnsymbol{footnote}}
\footnotetext[2]{
    Kazan Branch of Joint Supercomputer Center,
    Russian Academy of Sciences,
    Lobachevsky st. 2/31, 420111 Kazan, Russian Federation
    (\email{dennis.demidov@gmail.com})}
\footnotetext[3]{
    Kazan Federal University,
    Kremlyovskaya st. 18, 420008 Kazan, Russian Federation}
\renewcommand{\thefootnote}{\arabic{footnote}}

\begin{abstract}
    Algebraic multigrid (AMG) is one of the most effective iterative methods
    for solution of large sparse linear systems arising, for example, from
    discretizing PDEs on unstructured grids. The AMGCL library, which is
    presented in this work, provides an efficient, flexible, and extensible
    implementation of the algorithm in form of opensource, header-only \CXX
    library. It supports several computational backends, such as OpenMP,
    OpenCL, or NVIDIA CUDA, has minimal set of dependencies, and may be easily
    adapted to use custom data structures and operations.
\end{abstract}

\begin{keywords}
    Algebraic multigrid, \CXX, OpenMP, OpenCL, CUDA
\end{keywords}

\begin{AMS}
    35-04, 65-04, 65Y05, 65Y10, 65Y15, 97N80
\end{AMS}

\section{Introduction}

The capability to solve large and sparse systems of equations is a cornerstone
of modern numerical methods, sparse linear systems of equations being
ubiquitous in engineering and physics.  Direct techniques, despite their
attractiveness, simply become not viable beyond a certain size, typically of
the order of the few millions of unknowns (see e.g. \cite{hogg2013new,
henon2002pastix}), due to their intrinsic memory requirements and shear
computational cost.

Hence, preconditioned iterative methods become the only feasible alternative
for solution of large scale problems. Such methods typically fall in the class
of Krylov-subspace methods \cite{saad2003iterative}, and one of the most
popular preconditioning choices is the algebraic multigrid (AMG) method
\cite{Stuben1999, Trottenberg2001}.  The AMG can be used as a black box solver
for various computational problems, since it does not require any information
about the underlying geometry, and is known to be robust and scalable
\cite{cleary2000robustness}.

There are several well-known implementations of AMG available today. Notable
examples are ML package from Trilinos \cite{ml-guide}, BoomerAMG from Hypre
\cite{falgout2002hypre}, GAMG from PETSC \cite{petsc-user-ref}. These packages
are provided as parts of complex frameworks which target large distributed
memory machines, have a steep learning curve in general, and may be difficult
to compile (and even more difficult to distribute) on a user workstation.
Another problem is that most of the available packages are licensed under GPL,
and so are usually deemed inappropriate for use in commercial software. Being
as large and inert as they are, the frameworks are usually slow to implement
support for modern hardware, such as CUDA of OpenCL based GPUs. There are
smaller packages that address this issue. For example, the CUSP library
provides implementation of smoothed aggregation multigrid \cite{Cusp,
bell2012cuspamg}, but only supports NVIDIA hardware. The ViennaCL compute
library \cite{ViennaCL} extends the ideas and techniques presented in the CUSP
library to support OpenMP and OpenCL backends.

The AMGCL library tries to address the above issues. It has minimal set of
dependencies (that is, it only depends on Boost
libraries\footnote{\www{http://www.boost.org}} and on whatever the selected
backend depends on), is
distributed\footnote{\www{https://github.com/ddemidov/amgcl}} under permissive
MIT license, and targets shared memory machines and modern many-core
architectures. The library decouples setup and solution phases, and allows to
easily adapt the provided algorithms to work with user-defined data structures
and operations.  This paper provides an overview of AMGCL design, and compares
performance of the library with common alternatives.

\section{Design of AMGCL}

AMGCL is designed in a generic and extensible way that allows one to easily
construct an AMG variant from available algorithmic components of the method,
such as coarsening strategy or relaxation technique. The constructed hierarchy
then is transferred to one of supported backends for the solution phase.
Parallelization technologies such as OpenMP (for use with multicore CPUs), CUDA
or OpenCL (for use with modern GPUs) are supported through different backends.
The users of the library may easily implement their own backends in order for
the library to work transparently with custom data structures and algorithms.

\begin{lstlisting}[float=t,
caption=Solving linear system with AMGCL, label=lst:hello]
// Assemble system matrix in CRS format:
std::vector<int> ptr, col;
std::vector<double> val;
int n = assemble(ptr, col, val);

// Select the backend to use:
typedef amgcl::backend::builtin<double> Backend; $\label{line:backend}$

// Define solver components:
typedef amgcl::make_solver<  $\label{line:solver:def:begin}$
        amgcl::amg<                     // preconditioner:
            Backend,
            amgcl::coarsening::smoothed_aggregation,
            amgcl::relaxation::damped_jacobi $\label{line:relaxation}$
            >,
        amgcl::solver::cg<Backend>      // iterative solver:
    > Solver;  $\label{line:solver:def:end}$

// Set solver parameters:
Solver::params prm; $\label{line:prm:begin}$
prm.solver.tol = 1e-6;
prm.precond.relax.damping = 0.8; $\label{line:prm:end}$

// Setup solver:
Solver solve(boost::tie(n, ptr, col, val), prm); $\label{line:setup}$

// The RHS and the solution vectors:
std::vector<double> rhs(n, 1.0), x(n, 0.0); $\label{line:vectors}$

// Solve the system for the given RHS and initial solution:
int    iters;
double error;
boost::tie(iters, error) = solve(rhs, x); $\label{line:solve}$
\end{lstlisting}

\Cref{lst:hello} shows a basic example of using the AMGCL library.
Line~\ref{line:backend} selects the backend to use. Here we use the builtin
backend with \code{double} as a value type.
Lines~\ref{line:solver:def:begin}--\ref{line:solver:def:end} define the solver
type that we will use. The \code{make_solver} class binds together two
concepts: a preconditioner (in this case an \code{amg} class), and an iterative
solver (\code{cg}). Lines~\ref{line:prm:begin}--\ref{line:prm:end} show how to
set solver parameters. Here we set the desired relative residual norm and a
damping parameter for the damped Jacobi relaxation algorithm chosen in line
\ref{line:relaxation}. Most of the parameters have reasonable default values,
so we only need to change what is necessary. An instance of the solver is
constructed for the CRS matrix in line \ref{line:setup}. The instance is then
used to solve the system for the given right-hand side in
line~\ref{line:solve}.

Now, if we decided to use another backend, for example, the NVIDIA CUDA one, we
would only need to change the definition of the \code{Backend} type in line
\ref{line:backend} from \code{amgcl::backend::builtin<double>} to
\code{amgcl::backend::cuda<double>} and change the type of the 
vectors \code{rhs} and \code{x} from
\code{std::vector<double>} to \code{thrust::device_vector<double>} in
line~\ref{line:vectors}. As a result, the solver data would be transfered to
the supported GPU upon construction, and the solution would be performed
on the GPU. In a similar way, to change any of the algorithmic components of
the constructed solver, we just need to adjust its type definition in
lines~\ref{line:solver:def:begin}--\ref{line:solver:def:end}.

This simple example already exposes some of the library design choices. AMGCL
uses policy-based design \cite{alexandrescu2001modern} for the classes like
\code{make_solver} or \code{amg}, so that individual algorithmic components of
the classes are selected by the corresponding template parameters. The most
important concepts of the library are value types, backends, iterative solvers,
and preconditioners. The most important preconditioner that the library
provides is obviously \code{amgcl::amg}, which further depends on concepts of
coarsening and relaxation.

\subsection{Backends and Value Types}

A \emph{backend} in AMGCL is a class (template) that defines matrix and vector
types to be used during the solution step. It also specification some common
operations with the types, such as matrix vector product, residual, or linear
vector combinations. A backend also specifies how to do conversion from
internal AMGCL matrix and vector types into the backend-specific types. This
way the setup and the solution phases of the algorithm are completely
decoupled, and users of the library may easily provide their own backends.

A backend class is usually parameterized on a value type, or a type of the
system matrix elements. In the simplest case the value type may be just a
\code{double}, but it is also possible to use non-trivial types, such as
complex numbers or statically sized small matrices. To make this possible, the
value type should specialize the value type interface, which includes such
operations as inversion, norm, adjoint, etc. Using non-scalar types may improve
both computational and algorithmic efficiency of the method. For example, when
the system matrix has block structure with small dense blocks of fixed size,
then using block value may result in better convergence rate, improve
setup speed, and decrease cost of single iteration. See \cref{sec:performance}
for an example of this effect.

\subsection{Algorithmic Components}

\section{Performance Results} \label{sec:performance}

In this section the performance of the AMGCL library is compared with
alternatives based on two examples. The first one is obtained through finite
difference discretization of the Poisson problem in a 3D unit cube:
\begin{equation} \label{eq:poisson}
    -\Delta u = 1.
\end{equation}
The resulting linear system contains 3\,375\,000~unknowns and
23\,490\,000~nonzeros.

The second example is the incompressible Navier-Stokes problem discretized on a
non uniform 3D mesh with a finite element method:
\begin{subequations} \label{eq:block4}
\begin{gather}
    \frac{\partial \mathbf u}{\partial t} + \mathbf u \cdot \nabla \mathbf u  +
    \nabla p = \mathbf b, \\
    \nabla \cdot \mathbf u = 0.
\end{gather}
\end{subequations}

The system was assembled in Kratos\footnote{\www{http://www.cimne.com/kratos}}
multiphysics package \cite{dadvand2010kratos}, and graciously provided by Dr.
Riccardo Rossi for the tests. The matrix has block structure with blocks of
4-by-4 elements, and contains 713\,456~unknowns and 41\,277\,920~nonzeros.

The tests were performed on a dual socket system with two Intel Xeon E5-2640 v3
CPUs. The system also had an NVIDIA Tesla K80 GPU installed, which was used for
testing of the GPU based implementations.  Source code for these examples along
with the complete data sets are published at
\www{https://github.com/ddemidov/amgcl\_paper}. 

Scalability results for solution of problems \cref{eq:poisson,eq:block4} are
presented on \cref{fig:scalability}.  The libraries tested are AMGCL, Trilinos,
and PETSC.  AMGCL is using the builtin OpenMP backend, while Trilinos and PETSC
use MPI for parallelization.

\begin{figure}
    \begin{center}
        \includegraphics[width=\textwidth]{perf_plot}
    \end{center}
    \caption{Scalability of the considered libraries on a single node.}
    \label{fig:scalability}
\end{figure}

The left-hand side plots correspond to the Poisson problem \cref{eq:poisson}.
All three libraries use the Conjugate Gradient iterative solver
\cite{barrett1994templates} preconditioned with a smoothed aggregation AMG.
Trilinos and PETSC use default options for smoothers (symmetric Gauss-Seidel
and damped Jacobi accordingly) on each level of the hierarchy, and AMGCL uses
SPAI0 \cite{broker2002spai}.  The results show that AMGCL performs on par with
Trilinos, and both of the libraries outperform PETSC by a large margin. Also,
AMGCL is able to setup the solver about 20--100\% faster than Trilinos, and
4--7 times faster than PETSC.  This is probably due to the fact that both
Trilinos and PETSC target distributed memory machines and hence need to do some
complicated bookkeeping under the hood. PETSC shows better scalability than
both Trilinos and AMGCL, which scale in a similar fashion.

The right-hand side plots depict scalability of the Navier-Stokes problem
\cref{eq:block4}. The problem is a coupled system of four partial differential
equations, and the resulting matrix has block structure with blocks of four by
four elements. There are at least two ways to solve the system. First, one can
treat the system as a monolythic one, and provide some minimal help to the
preconditioner in form of near null space vectors. Second option is to employ
the knowledge about the problem structure, and to combine separate
preconditioners for individual fields (in this particular case, for pressure
and velocity). The first option on \cref{fig:scalability} is marked as
\lq{block}\rq, and the second option~--- as \lq{split}\rq. In case of AMGCL both
options were tested; Trilinos ML only provides the first option; and PETSC
could do both, but only results for the second, superior option, are presented
here. Additionally, \lq{AMGCL (scalar)}\rq version corresponds to solution of
the coupled system, but with scalar value type used instead of the block one.

\begin{table}
    \caption{Performance of different libraries/backends with full utilization
    of a single compute node.}
    \label{tab:gpuperf}
    \begin{center}
        \input{perf_table} % generated by data/table.py:
    \end{center}
\end{table}

\section{Conclusion}

\section{Aknowledgements}

The work was partially supported by the Russian Foundation for Basic Research
grants No 15-47-02343 and 15-07-05380.

\bibliographystyle{siam}
\bibliography{ref}

\end{document}
