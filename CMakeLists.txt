cmake_minimum_required(VERSION 2.8)
project(amgcl)

find_package(LATEX)
if (LATEX_FOUND)
    include(UseLATEX.cmake)
    add_latex_document(amgcl.tex
        BIBFILES ref.bib
        INPUTS perf_table.tex
        IMAGES perf_plot.pdf
        DEPENDS siamart1116.cls siamplain.bst
        )

    set_source_files_properties(perf_table.tex PROPERTIES GENERATED TRUE)
    set_source_files_properties(perf_plot.pdf PROPERTIES GENERATED TRUE)
    add_subdirectory(data)
endif()

option(BUILD_CODE "Build code examples" OFF)
if (BUILD_CODE)
    add_subdirectory(code)
endif()
